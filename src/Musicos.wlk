/** First Wollok example */
object joaquin {

	var grupo = "Pimpinela"
	var habilidadInicial = 20
	var cantaEnGrupo;
	
	method habilidad(){
		var hab = habilidadInicial
		if(cantaEnGrupo){
			hab +=5
		}
		return hab
	}
	
	method interpretaBien(cancion) = cancion.duracion() > 300

	
	method costo(){

		if(cantaEnGrupo){
			return 100
		} 
		return 50
	}
	
	
}

object lucia {
	
	var grupo = "Pimpinela"
	 
	var habilidadInicial = 70
	var cantaEnGrupo;
	
	method habilidad(){
		var hab = habilidadInicial
		if(cantaEnGrupo){
			hab -= 20
		}
		return hab
	}
	
	method interpretaBien(cancion){
		
	} 
	
	method costo(lugar){
		var costo;
		if(lugar.concurrido()){ costo =  500 }else{ costo =  400} return costo
		
	}
	
}

object luisAlberto {
	
	var guitarra;
	
	
	method habilidad(){
		
		var val = 8*guitarra.valor()
		return val.min(100) 
		
	}
	method interpretaBien(_cancion) = true
	
	method costo(_lugar){
		/* Date < septiembre 2017 ? 1000 : 1200 */
		
	}
	
}


object cisne{
	
	method duracion()= 312
	method letra() = "Hoy el viento se abrió quedó vacío 
			el aire una vez más y el manantial brotó 
			y nadie está aquí y puedo ver que solo 
			estallan las hojas al brillar"
			
			
	
}

object laFamilia{
	method duracion()= 264
	method letra() = "Quiero brindar por mi gente sencilla, por el amor brindo por la familia"
}









